package com.galid.teststudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeststudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeststudyApplication.class, args);
    }

}
